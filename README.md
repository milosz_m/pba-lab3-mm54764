# <div align="center">SOAP/REST API do Zarządzania Użytkownikami</div>
Data: 25.11.2023

## Zad 1

Aplikacja Spring w pliku `spring-app.zip` - akurat zadanie 1 pokazywalem na zajeciach.
Plik `users.xsd` wyciagniety z repozytorium w razie potrzeby przejrzenia.

## Zad 2

Przykładowe API do zarządzania użytkownikami - opis pliku `open-api.yaml`.

**Wersja:** 1.0.0

**Regulamin:** [http://swagger.io/terms/](http://swagger.io/terms/)

**Licencja:** [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

## Bazowy URL API

- **Host:** localhost:8080
- **Ścieżka Bazowa:** /v1

## Tagi

- **user:** Operacje związane z zarządzaniem użytkownikami

## Schematy

- HTTPS
- HTTP

## Ścieżki

### Tworzenie Nowego Użytkownika

- **Metoda HTTP:** POST
- **Ścieżka:** /users
- **Tagi:** user
- **Podsumowanie:** Tworzenie nowego użytkownika
- **ID Operacji:** createUser
- **Konsumuje:** application/json
- **Produkuje:** application/json
- **Parametry:**
  - Nazwa: body
    - W: body
    - Opis: Obiekt użytkownika, który ma być utworzony
    - Wymagane: true
    - Schema: [Definicja Użytkownika](#definicja-użytkownika)
- **Odpowiedzi:**
  - "201": Użytkownik został pomyślnie utworzony
  - "400": Błędne żądanie
- **Zabezpieczenia:**
  - BasicAuth

### Pobieranie Listy Użytkowników

- **Metoda HTTP:** GET
- **Ścieżka:** /users
- **Tagi:** user
- **Podsumowanie:** Pobieranie listy użytkowników
- **ID Operacji:** listUsers
- **Produkuje:** application/json
- **Odpowiedzi:**
  - "200": Lista użytkowników
    - Schema: [Definicja Użytkownika](#definicja-użytkownika)
  - "400": Błędne żądanie
- **Zabezpieczenia:**
  - BasicAuth

### Pobieranie Użytkownika po ID

- **Metoda HTTP:** GET
- **Ścieżka:** /users/{userId}
- **Parametry:**
  - Nazwa: userId
    - W: path
    - Opis: ID użytkownika
    - Wymagane: true
    - Typ: integer
    - Format: int64
- **Tagi:** user
- **Podsumowanie:** Pobieranie użytkownika po ID
- **ID Operacji:** getUserById
- **Produkuje:** application/json
- **Odpowiedzi:**
  - "200": Szczegóły użytkownika
    - Schema: [Definicja Użytkownika](#definicja-użytkownika)
  - "404": Użytkownik nie znaleziony
- **Zabezpieczenia:**
  - OAuth2 (Uprawnienia: read, write)

### Aktualizowanie Użytkownika po ID

- **Metoda HTTP:** PUT
- **Ścieżka:** /users/{userId}
- **Parametry:**
  - Nazwa: userId
    - W: path
    - Opis: ID użytkownika do aktualizacji
    - Wymagane: true
    - Typ: integer
    - Format: int64
  - Nazwa: body
    - W: body
    - Opis: Zaktualizowany obiekt użytkownika
    - Wymagane: true
    - Schema: [Definicja Użytkownika](#definicja-użytkownika)
- **Tagi:** user
- **Podsumowanie:** Aktualizowanie użytkownika po ID
- **ID Operacji:** updateUserById
- **Konsumuje:** application/json
- **Produkuje:** application/json
- **Odpowiedzi:**
  - "200": Użytkownik został pomyślnie zaktualizowany
  - "400": Błędne żądanie
- **Zabezpieczenia:**
  - OAuth2 (Uprawnienia: read, write)

### Usuwanie Użytkownika po ID

- **Metoda HTTP:** DELETE
- **Ścieżka:** /users/{userId}
- **Parametry:**
  - Nazwa: userId
    - W: path
    - Opis: ID użytkownika do usunięcia
    - Wymagane: true
    - Typ: integer
    - Format: int64
- **Tagi:** user
- **Podsumowanie:** Usuwanie użytkownika po ID
- **ID Operacji:** deleteUserById
- **Produkuje:** application/json
- **Odpowiedzi:**
  - "204": Użytkownik został pomyślnie usunięty
  - "404": Użytkownik nie znaleziony
- **Zabezpieczenia:**
  - OAuth2 (Uprawnienie: delete)

## Definicja Użytkownika

- **Typ:** obiekt
- **Właściwości:**
  - id: string (Format: uuid)
  - firstName: string (Minimalna Długość: 2, Maksymalna Długość: 30)
  - lastName: string (Minimalna Długość: 2, Maksymalna Długość: 30)
  - age: number (Maksymalna Wartość: 999)
  - pesel: integer (Minimalna Długość: 11, Maksymalna Długość: 11)
  - nationality: string (Enum: PL, DE, UK)
  - email: string (Format: email)
- **Wymagane Właściwości:**
  - firstName
  - lastName
  - age
  - pesel
  - nationality
  - email

## Definicje Zabezpieczeń

- **BasicAuth:**
  - Typ: basic

- **OAuth2:**
  - Typ: oauth2
  - Flow: implicit
  - URL Autoryzacji: [http://localhost:8080/oauth2/authorize](http://localhost:8080/oauth2/authorize)
  - Zakresy (Scopes):
    - read: Dostęp do odczytu
    - write: Dostęp do zapisu

---
<div align="center"><strong>Miłosz Misiek</strong></div>
